## Que hacer con estos archivos

#Para implementar el tema de kronops:
Los logos en formato png deben colocarse en el directorio /var/www/kronops.mx/htdocs/images y sustituir los archivos con los mismos nombres.

La hoja de estilo light-grey.css debe sustituir a la del directorio /var/www/kronops.mx/htdocs/css, la de nombre jquery-ui-1.11.4.custom.css va en el directorio /var/www/Kronops.mx/htdocs/css/ui-lightness

##Para Personalizar un tema para otros proyectos:

En la hoja de estilos /var/www/DOMINIO/htdocs/css/light-grey.css hacer una sustitucion de ea7d1e por el color de identidad de la empresa en hexadecimal

Ingresar al theme roller http://jqueryui.com/themeroller/#!zThemeParams=5d000001001306000000000000003d8888d844329a8dfe02723de3e5701dc2cb2be0d98fe676bb46e85f3b85ff2d347a9c5170a6c17a4a3d926b08b9d199c4e573fcbf9cc1a2dd092a9d80b65cc27a41c959e1c11c3f58747718755b7fc90eb73b9c06df29f308cd293b47a7ab61c1e5e9b99171b192d897d32f2fed51436538e0c4e02cd8f00a329be8244ddaa595637096852af8f5ce61a21a780499b0ed0ca7693f4e7eb0d5c3dbb32500a6d03123e05485eeb24246800a86a33d0ba0fc5ad76239359a9fa0e11b145581ee3ba94228949acca1b006a8bdea53a939739fb7d3b51d6dd555bc71cc920f370708f383e980feb0e332b2f8301c68308df2ede1769ef7391fb45409e63f6cba1167c9f01dc57e6c15ff42410c9f4a4930e8623ec961bdcbac852cc0e829d43309705f0058b6d7c3ccdb1a15a759ed07738091439130ca18c88ee310d082f0d657512b6915cb64e0627ed9d2743a841e79cbd14539fb142b4ceca5048c954d3e9f48d0c68d867883a4205f56fbc3b40331c15e14fc9e28fb9cfffd20705255723d647d5ffa16769311cef15eaf32aa9ce82707a1160686dba5450cf18440b0d95e53012c2928698bdbcd545830863fe40c9c0ea477cfbd725189118130f3373afad41accb0b52248b591e41765658f16e1eb02e749b91d27c5fb7fef691d575471d4eb17e89bffb218fa1c
Modificar el tema
descargar el tema
renombrar jquery-ui.min.css como jquery-ui-1.11.4.custom.css
sustituir el archivo /var/www/DOMINIO/htdocs/css/jquery-ui-1.11.4.custom.css por el que hemos generado con roller theme

Los logos deben tener una orientacion landscape, y estar en formato jpg con una resolucion de 113 * 54 px.
Se recomienda usar la herramienta http://resizeimage.net/ la cual convierte a png y redimenciona la imagen.
-en las opciones de resize image quitamos keep aspect ratio y damos los valores 113 y 54, seleccionamos fill background en transparent, y damos png como opcion de output
-el archivo generado lo renombramos como itop-logo-external.png y hacemos una copia con nombre itop-logo.png
los subimos a nuestro servidor a la carpeta /var/www/DOMINIO/htdocs/images
